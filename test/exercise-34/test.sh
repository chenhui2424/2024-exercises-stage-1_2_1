#!/usr/bin/env bash

EXECUTABLE=$1

KO=$(readelf -s ${EXECUTABLE} | grep FILE | sed '1d' | awk '{print $8}')

insmod $EXECUTABLE ls_array_input=3,6,9,12,15 ls_target=12

MODULE_MESSAGE=$(dmesg | grep "Index of 12 in the array: 3")

rmmod ${KO%.c}

if [ ! -z "$MODULE_MESSAGE" ]; then
  echo "Test passed."
  exit 0
else
  echo "Test failed. Expected have '$MODULE_MESSAGE' but got empty"
  exit 1
fi
