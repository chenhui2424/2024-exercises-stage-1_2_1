#!/usr/bin/env bash

EXECUTABLE=$1

KO=$(readelf -s ${EXECUTABLE} | grep FILE | sed '1d' | awk '{print $8}')

insmod $EXECUTABLE average_input=5,10,15,20,25

MODULE_MESSAGE=$(dmesg | grep "Average of the array: 15")

rmmod ${KO%.c}

if [ ! -z "$MODULE_MESSAGE" ]; then
  echo "Test passed."
  exit 0
else
  echo "Test failed. Expected have '$MODULE_MESSAGE' but got empty"
  exit 1
fi
